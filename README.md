#JWT DISCO | [October CMS](https://octobercms.com/)

##Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

#Prerequisites

Below are requirements your local machine needs:

1. Download [Composer](http://getcomposer.org/doc/00-intro.md).
2. PHP 7.0 or higher with PDO, cURL, OpenSSL, Mbstring, ZipArchive, and GD extension.
3. HTTP Server.
4. Database Engine.

#Installing

Below are step by step series of examples you need to do:

1. Clone repository.
2. Run `composer install`. Allow folder permissions if composer asks.
3. Enable DotEnv configuration using `php artisan october:env` and setup according to local environment.
4. Migrate database using `php artisan october:up`.
5. Set active theme to JWT Disco theme using `php artisan theme:use jwtdisco`.
6. Run server using `php artisan serve`.