<?php

use Mirum\Prevstars\Models\Prevstar;

Route::get('prevstar/seed', function () {
    $faker = Faker\Factory::create();
    $level = ['Jr. ', 'Sr. ', ''];
    $job_title = ['Developer', 'Technical Writter', 'Project Manager', 'Designer', 'Infrastructure', 'Account Executive'];
    
    for($i = 0; $i < 8; $i++){
        $name = $faker->name;
        Prevstar::create([
            'name' => $faker->name,
            'job_title' => $level[array_rand($level)] . $job_title[array_rand($job_title)],
            'is_finalist' => true,
            'is_winner' => false
        ]);
    }

    for($i = 0; $i < 2; $i++){
        $name = $faker->name;
        Prevstar::create([
            'name' => $faker->name,
            'job_title' => $level[array_rand($level)] . $job_title[array_rand($job_title)],
            'is_finalist' => true,
            'is_winner' => true
        ]);
    }

    return "Prev-Stars succesfully seeded";
});

?>