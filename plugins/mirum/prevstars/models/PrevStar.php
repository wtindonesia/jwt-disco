<?php namespace Mirum\Prevstars\Models;

use Model;

/**
 * Model
 */
class PrevStar extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mirum_prevstars_';

    protected $fillable = [
        'name', 'job_title', 'is_finalist', 'is_winner'
    ];
}
