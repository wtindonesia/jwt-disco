<?php namespace Mirum\Prevstars;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public function pluginDetails()
    {
        return [
            'name'        => 'PrevStars',
            'description' => 'Provides features used for prevstars.',
            'author'      => 'Mirum',
            'icon'        => 'icon-star-half-o'
        ];
    }

    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }
}
