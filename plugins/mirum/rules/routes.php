<?php

use Mirum\Rules\Models\Rule;

Route::get('rule/seed', function () {
    $faker = Faker\Factory::create();
    $title = ['Participant', 'Registration', 'Qualification', 'Final Round', 'Mechanism', 'Entry Fee'];
    for($i = 0; $i < 6; $i++){
        Rule::create([
            'title' => $title[$i],
            'slug' => str_slug($title[$i], '-'),
            'description' => $faker->text($maxNbChars = 200),
            'order' => $i+1
        ]);
    }

    return "Rules created";
});

?>