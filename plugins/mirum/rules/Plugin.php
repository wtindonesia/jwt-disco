<?php namespace Mirum\Rules;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public function pluginDetails()
    {
        return [
            'name'        => 'Rules',
            'description' => 'Provides features used for rules.',
            'author'      => 'Mirum',
            'icon'        => 'icon-list-ol'
        ];
    }

    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }
}
