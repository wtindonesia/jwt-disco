<?php namespace Mirum\Stars;

use System\Classes\PluginBase;
use Rainlab\User\models\User as UserModel;
use Rainlab\User\Controllers\Users as UsersController;

class Plugin extends PluginBase
{
    public $require = ['RainLab.User'];

    public function pluginDetails()
    {
        return [
            'name'        => 'Stars',
            'description' => 'Provides extended features used from user plugin.',
            'author'      => 'Mirum',
            'icon'        => 'icon-star'
        ];
    }

    public function registerComponents()
    {
        return [
            'Mirum\Stars\Components\Star' => 'Star'
        ];
    }

    public function registerReportWidgets()
    {
        return [
            'Mirum\Stars\ReportWidgets\OverallStar' => [
                'label'   => 'Get Overall Data of Star',
                'context' => 'dashboard'
            ],
            'Mirum\Stars\ReportWidgets\NewestStar' => [
                'label'   => 'Get Top Twenty Newest Star',
                'context' => 'dashboard'
            ]
        ];
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
        UserModel::extend(function($model) {

            $model->addFillable([
                'dob',
                'job_title',
                'university',
                'mobile',
            ]);

            $model->attachOne = [
                'avatar' => \System\Models\File::class,
                'campus_letter' => \System\Models\File::class,
                'payment_receipt' => \System\Models\File::class,
                'submit_work' => \System\Models\File::class
            ];

            $model->addDynamicMethod('scopeNewestTen', function($query) {
                return $query->orderBy('created_at', 'desc')->take(10);
            });
            $model->addDynamicMethod('scopeIsInnactivated', function($query) {
                return $query->where('is_activated', 0);
            });
        });
        
        UsersController::extendFormFields(function($form, $model, $context){
            $form->addTabFields([
                'dob' => [
                    'label' => 'Date of Birth',
                    'type' => 'datepicker',
                    'mode' => 'date',
                    'default' => 'now',
                    'format' => 'Y M d',
                    'minDate' => '1945-01-01',
                    'maxDate' => 'now',
                    'span' => 'auto',
                    'tab' => 'Profile'
                ],
                'mobile' => [
                    'label' => 'Mobile Phone',
                    'type' => 'text',
                    'span' => 'auto',
                    'tab' => 'Profile',
                ],
                'job_title' => [
                    'label' => 'Job Title',
                    'type' => 'text',
                    'span' => 'auto',
                    'tab' => 'Profile'
                ],
                'university' => [
                    'label' => 'University',
                    'type' => 'text',
                    'span' => 'auto',
                    'tab' => 'Profile'
                ],
                'campus_letter' => [
                    'label' => 'Campus Letter',
                    'type' => 'fileupload',
                    'mode' => 'file',
                    'tab' => 'File'
                ],
                'payment_receipt' => [
                    'label' => 'Payment Receipt',
                    'type' => 'fileupload',
                    'mode' => 'file',
                    'tab' => 'File'
                ],
                'submit_work' => [
                    'label' => 'Submit Work',
                    'type' => 'fileupload',
                    'mode' => 'file',
                    'tab' => 'File'
                ],
                
            ]);
        });
    }
}
