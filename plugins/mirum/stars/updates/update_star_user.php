<?php namespace Mirum\Stars\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateStarUser extends Migration
{

    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->string('mobile')->after('password');
            $table->string('university')->after('password');
            $table->string('job_title')->after('password');
            $table->date('dob')->after('password');
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn([
                'job_title', 'university', 'mobile', 'dob'
            ]);
        });
    }

}
