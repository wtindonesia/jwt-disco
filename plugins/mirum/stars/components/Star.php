<?php namespace Mirum\Stars\Components;

use Lang;
use Auth;
use Mail;
use Event;
use Flash;
use Input;
use Request;
use Redirect;
use Validator;
use ValidationException;
use ApplicationException;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use RainLab\User\Models\Settings as UserSettings;
use Exception;
use System\Models\File;
use RainLab\User\Components\Account as UserAccountComponent;

class Star extends UserAccountComponent
{
    public function componentDetails()
    {
        return [
            'name'        => 'Stars',
            'description' => 'User management form.'
        ];
    }

    /**
     * Sign in the user
     */
    public function onSigninStar()
    {
        try {
            /*
             * Validate input
             */
            $data = post();
            $rules = [];

            $rules['login'] = $this->loginAttribute() == UserSettings::LOGIN_USERNAME
                ? 'required|between:2,255'
                : 'required|email|between:6,255';

            $rules['password'] = 'required|between:4,255';

            if (!array_key_exists('login', $data)) {
                $data['login'] = post('username', post('email'));
            }

            $validation = Validator::make($data, $rules);
            if ($validation->fails()) {
                return ['#result' => $this->renderPartial('@signin_messages.htm', [
                    'errorMsgs' => $validation->messages()->all(),
                    'fieldMsgs' => $validation->messages()
                ])];
            }

            /*
             * Authenticate user
             */
            $credentials = [
                'login'    => array_get($data, 'login'),
                'password' => array_get($data, 'password')
            ];

            Event::fire('rainlab.user.beforeAuthenticate', [$this, $credentials]);

            $user = Auth::authenticate($credentials, true);
            if ($user->isBanned()) {
                Auth::logout();
                throw new AuthException(/*Sorry, this user is currently not activated. Please contact us for further assistance.*/'rainlab.user::lang.account.banned');
            }

            /*
             * Redirect
             */
            if ($redirect = $this->makeRedirection(true)) {
                return $redirect;
            }
        }
        catch (Exception $ex) {
            if (Request::ajax()) throw $ex;
            else Flash::error($ex->getMessage());
        }
    }

    /**
     * Register the user
     */
    public function onRegisterStar()
    {
        try {
            if (!$this->canRegister()) {
                throw new ApplicationException(Lang::get(/*Registrations are currently disabled.*/'rainlab.user::lang.account.registration_disabled'));
            }

            /*
             * Validate input
             */
            $data = post();

            if (!array_key_exists('password_confirmation', $data)) {
                $data['password_confirmation'] = post('password');
            }

            // if (Input::hasFile('avatar')) {
            $data['avatar'] = Input::file('avatar');
            $data['campus_letter'] = Input::file('campus_letter');
            $data['payment_receipt'] = Input::file('payment_receipt');
            $data['submit_work'] = Input::file('submit_work');
            // }

            $rules = [
                'name'    => 'required|between:2,255',
                'email'    => 'required|email|unique:users|between:6,255',
                'password' => 'required|between:4,255|confirmed',
                'name'    => 'required|between:2,255',
                'dob'    => 'required|date',
                'job_title' => 'required|',
                'university' => 'required',
                'mobile' => 'required|numeric',
                'avatar'   => 'required|image|max:4000',
                'campus_letter'   => 'required|mimes:pdf|max:10000',
                'payment_receipt'   => 'required|mimes:pdf|max:10000',
                'submit_work'   => 'required|mimes:pdf|max:10000',
            ];

            if ($this->loginAttribute() == UserSettings::LOGIN_USERNAME) {
                $rules['username'] = 'required|between:2,255';
            }

            $validation = Validator::make($data, $rules);
            if ($validation->fails()) {
                return ['#result' => $this->renderPartial('@register_messages.htm', [
                    'errorMsgs' => $validation->messages()->all(),
                    'fieldMsgs' => $validation->messages()
                ])];
            }

            /*
             * Register user
             */
            Event::fire('rainlab.user.beforeRegister', [&$data]);

            $requireActivation = UserSettings::get('require_activation', true);
            $automaticActivation = UserSettings::get('activate_mode') == UserSettings::ACTIVATE_AUTO;
            $userActivation = UserSettings::get('activate_mode') == UserSettings::ACTIVATE_USER;
            $user = Auth::register($data, $automaticActivation);

            if (Input::hasFile('avatar')) {
                $user->avatar = Input::file('avatar');
            }
            if (Input::hasFile('campus_letter')) {
                $user->campus_letter = Input::file('campus_letter');
            }
            if (Input::hasFile('payment_receipt')) {
                $user->payment_receipt = Input::file('payment_receipt');
            }
            if (Input::hasFile('submit_work')) {
                $user->submit_work = Input::file('submit_work');
            }

            Event::fire('rainlab.user.register', [$user, $data]);

            /*
             * Activation is by the user, send the email
             */
            if ($userActivation) {
                $this->sendActivationEmail($user);

                Flash::success(Lang::get(/*An activation email has been sent to your email address.*/'rainlab.user::lang.account.activation_email_sent'));
            }

            /*
             * Automatically activated or not required, log the user in
             */
            if ($automaticActivation || !$requireActivation) {
                Auth::login($user);
            }

            /*
             * Redirect to the intended page after successful sign in
             */
            if ($redirect = $this->makeRedirection(true)) {
                return $redirect;
            }
        }
        catch (Exception $ex) {
            if (Request::ajax()) throw $ex;
            else Flash::error($ex->getMessage());
        }
    }

    public function onImageUpload(){
        $data = Input::all();

        $file = (new File())->fromPost($data['avatar']);

        return[
            '#imageResult' => '<img src="' . $file->getThumb(200,200, ['mode' => 'crop']) . '">'
        ];
    }
}